export class ParteRicambio {
  idParte: number;
  matricola: string;
  descParte: string;
  stato: string;
  aggiornato: boolean;
  condizioniParte: string;
  cliente: number;
}
