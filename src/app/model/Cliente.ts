export class Cliente {
  idCliente: number;
  descrizione: string;
  email: string;
  telefono: string;
  indirizzo: boolean;
  stato: string;
}
