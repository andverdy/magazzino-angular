import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Cliente } from "../model/Cliente";

@Injectable({
  providedIn: "root",
})
export class ClientiService {
  constructor(private httpClient: HttpClient) {}
  token = sessionStorage.getItem("token");
  getClienti() {
    let httpHeaders = new HttpHeaders().set("authorization", this.token);
    return this.httpClient.get<Array<Cliente>>(
      "http://localhost:8080/magazzino/api/cliente/list",
      {
        headers: httpHeaders,
        responseType: "json",
      }
    );
  }

  getClienteById(id: number): Observable<any> {
    //let params = new HttpParams().set("id", id);

    let httpHeaders = new HttpHeaders().set("Authorization", this.token);

    return this.httpClient.get(
      "http://localhost:8080/magazzino/api/cliente/findBy-id" + "?id=" + id,
      {
        headers: httpHeaders,
        responseType: "json",
      }
    );
  }

  eliminaClienteById(id: number): Observable<any> {
    //let params = new HttpParams().set("id", id);

    let httpHeaders = new HttpHeaders().set("Authorization", this.token);

    return this.httpClient.get(
      "http://localhost:8080/magazzino/api/cliente/delete" + "?id=" + id,
      {
        headers: httpHeaders,
        responseType: "json",
      }
    );
  }

  aggiorna(cliente: Cliente) {
    let httpHeaders = new HttpHeaders().set("authorization", this.token);
    return this.httpClient.put<Cliente>(
      "http://localhost:8080/magazzino/api/cliente/save",
      cliente,
      { headers: httpHeaders }
    );
  }
}
