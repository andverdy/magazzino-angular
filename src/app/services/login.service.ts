import { Injectable, Output } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Router } from "@angular/router";
import { User } from "../model/User";
import { Observable } from "rxjs";

@Injectable()
export class LoginService {
  isUserLogged = false;

  constructor(private httpClient: HttpClient, private router: Router) {}

  registrati(username: string, password: string) {
    let body = {
      username: username,
      password: password,
    };
    return this.httpClient.post<User>(
      "http://localhost:8080/magazzino/api/register",
      body,
      { responseType: "json" }
    );
  }

  autenticaService(username: string, password: string) {
    let body = {
      username: username,
      password: password,
    };
    return this.httpClient
      .post("http://localhost:8080/magazzino/api/authenticate", body, {
        responseType: "json",
      })
      .pipe(
        map((data) => {
          sessionStorage.setItem("username", username);
          let tokenStr = "Bearer " + Object.keys(data).map((key) => data[key]);
          sessionStorage.setItem("token", tokenStr);
          console.log(data);
        })
      );
  }

  getIdByUsername(username: string): Observable<any> {
    return this.httpClient.get(
      "http://localhost:8080/magazzino/api/user/getId-byUsername?username=" +
        username,
      {
        responseType: "json",
      }
    );
  }

  saveRole(id: number) {
    return this.httpClient.post(
      "http://localhost:8080/magazzino/api/user/role-save?id=" + id,
      { responseType: "json" }
    );
  }

  isUserLoggedIn() {
    this.isUserLogged = !!sessionStorage.getItem("token");
    return this.isUserLogged;
  }
  loggedUser() {
    let utente = sessionStorage.getItem("username");

    return sessionStorage.getItem("username") != null ? utente : "";
  }

  getAuthToken() {
    if (this.loggedUser()) return sessionStorage.getItem("token");
    else return "";
  }

  isLogged() {
    return sessionStorage.getItem("username") != null ? true : false;
  }
}
