import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class UserService {
  username = sessionStorage.getItem("username");
  token = sessionStorage.getItem("token");
  constructor(private httpClient: HttpClient) {}

  roleIsAuth(): Observable<any> {
    let httpHeaders = new HttpHeaders().set("authorization", this.token);
    return this.httpClient.get(
      "http://localhost:8080/magazzino/api/user/roleIsAuth-byUsername" +
        "?username=" +
        this.username,
      {
        headers: httpHeaders,
        responseType: "json",
      }
    );
  }
}
