import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, pipe } from "rxjs";
import { ParteRicambio } from "../model/ParteRicambio";
import { catchError, map, tap } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class PartiServiceService {
  filename = "parti.xls";

  constructor(private httpClient: HttpClient) {}
  token = sessionStorage.getItem("token");
  getParti() {
    let httpHeaders = new HttpHeaders().set("authorization", this.token);
    return this.httpClient.get<Array<ParteRicambio>>(
      "http://localhost:8080/magazzino/api/parti-ricambio/list",
      {
        headers: httpHeaders,
        responseType: "json",
      }
    );
  }

  aggiorna(parte: ParteRicambio) {
    let httpHeaders = new HttpHeaders().set("authorization", this.token);
    return this.httpClient.put<ParteRicambio>(
      "http://localhost:8080/magazzino/api/parti-ricambio/save",
      parte,
      { headers: httpHeaders }
    );
  }

  eliminaParteById(idParte: number): Observable<number> {
    console.log("sono nel delete");
    //let params = new HttpParams().set("id", id);

    let httpHeaders = new HttpHeaders().set("Authorization", this.token);

    return this.httpClient.delete<number>(
      "http://localhost:8080/magazzino/api/parti-ricambio/deleteById" +
        "?idParte=" +
        idParte,
      {
        headers: httpHeaders,
      }
    );
  }

  getParteByMatricola(matricola: string): Observable<any> {
    let params = new HttpParams().set("matricola", matricola);

    let httpHeaders = new HttpHeaders().set("Authorization", this.token);

    return this.httpClient.get(
      "http://localhost:8080/magazzino/api/parti-ricambio/findBy-matricola",
      {
        headers: httpHeaders,
        responseType: "json",
        params: params,
      }
    );
  }

  downloadExcelFile(): void {
    const baseUrl = "http://localhost:8080/magazzino/api/download/excel-file";
    //const token = "my JWT";
    const headers = new HttpHeaders().set("authorization", this.token);
    this.httpClient
      .get(baseUrl, {
        headers,
        responseType: "blob" as "json",
      })
      .subscribe((response: any) => {
        let dataType = response.type;
        let binaryData = [];
        binaryData.push(response);
        let downloadLink = document.createElement("a");
        downloadLink.href = window.URL.createObjectURL(
          new Blob(binaryData, { type: dataType })
        );
        if (this.filename) downloadLink.setAttribute("download", this.filename);
        document.body.appendChild(downloadLink);
        downloadLink.click();
      });
  }
}
