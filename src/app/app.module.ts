import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./components/login/login.component";
import { LoginService } from "./services/login.service";
import { HttpClientModule } from "@angular/common/http";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { BasicAuthHtppInterceptorService } from "./services/BasicAuthHtppInterceptorService";
import { PartiServiceService } from "./services/parti-service.service";
import { PartiRicambioComponent } from "./components/parti-ricambio/parti-ricambio.component";
import { ClientiService } from "./services/clienti.service";
import { ClienteComponent } from "./components/cliente/cliente.component";
import { FormPartiComponent } from "./components/formParti/formParti.component";
import { FormClienteComponent } from "./components/form-cliente/form-cliente.component";
import { DownloadExcelComponent } from "./components/download-excel/download-excel.component";
import { UserService } from "./services/user.service";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    PartiRicambioComponent,
    ClienteComponent,
    FormPartiComponent,
    FormClienteComponent,
    DownloadExcelComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule, HttpClientModule],
  providers: [
    LoginService,
    BasicAuthHtppInterceptorService,
    PartiServiceService,
    ClientiService,
    UserService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
