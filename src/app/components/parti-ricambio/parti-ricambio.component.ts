import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { ParteRicambio } from "src/app/model/ParteRicambio";
import { ClientiService } from "src/app/services/clienti.service";
import { PartiServiceService } from "src/app/services/parti-service.service";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "app-parti-ricambio",
  templateUrl: "./parti-ricambio.component.html",
  styleUrls: ["./parti-ricambio.component.css"],
})
export class PartiRicambioComponent implements OnInit {
  @Input() parte: ParteRicambio;
  parti: Array<ParteRicambio> = [];
  result: number;
  //username = sessionStorage.getItem("username");
  sessionRoleIsAuth: string;
  roleIsAuth = sessionStorage.getItem("userRoleAuthString");

  constructor(
    private partiService: PartiServiceService,
    private router: Router,
    private clientiService: ClientiService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.userService
      .roleIsAuth()
      .subscribe((response) => this.handleResponseUser(response));

    if (this.roleIsAuth === "true") {
      console.log("DISATTIVA BOTTONE");
    }

    console.log("stampa idparte presa da input");

    this.partiService
      .getParti()
      .subscribe((response) => this.handleResponse(response));
  }

  gestClienti() {
    this.router.navigate(["clientiPage"]);
  }

  eliminaParte(idParte: number) {
    this.partiService.eliminaParteById(idParte).subscribe();

    alert("Parte din Ricambio Eliminata!");
    window.location.reload();
    //this.router.navigate(["partiPage"]);
  }

  visualizzaFormAggiornamento(matricola: string) {
    if (matricola != null) {
      sessionStorage.setItem("matricola", matricola);
      this.router.navigate(["/aggiornaParte"]);
    }
  }
  visualizzaFormInserimento() {
    sessionStorage.removeItem("matricola");
    this.router.navigate(["/aggiornaParte"]);
  }

  scaricaExcel() {
    this.router.navigate(["/downExcelPage"]);
  }

  handleResponse(response: Array<ParteRicambio>) {
    console.log(response);
    response.forEach((parte: ParteRicambio) => {
      this.parti.push(parte);
    });
  }

  handleResponseUser(response) {
    console.log("fammi vedere il boolean del metodo handle");
    console.log(response);
    this.roleIsAuth = response;
  }
}
