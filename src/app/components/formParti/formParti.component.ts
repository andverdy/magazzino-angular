import { Component, Input, OnInit } from "@angular/core";

import { Router } from "@angular/router";
import { ParteRicambio } from "src/app/model/ParteRicambio";
import { PartiServiceService } from "src/app/services/parti-service.service";

@Component({
  selector: "app-formParti",
  templateUrl: "./formParti.component.html",
  styleUrls: ["./formParti.component.css"],
})
export class FormPartiComponent implements OnInit {
  parte: ParteRicambio;
  isUpd = sessionStorage.getItem("matricola");
  insert: boolean;
  update: boolean;
  public h1: any = "Inserisci Parte Di Ricambio";

  constructor(
    private route: Router,
    private partiService: PartiServiceService
  ) {}

  ngOnInit(): void {
    let matricola = sessionStorage.getItem("matricola");

    if (matricola != null) {
      this.h1 = "Modifica Parte Di Ricambio";
      this.update = true;
      // this.parte = history.state.data;
      console.log("la parte esiste nel db quindi verrà aggiornata");
      this.partiService
        .getParteByMatricola(matricola)
        .subscribe((response) => this.handleResponseByMatr(response));
    } else {
      this.h1 = "Inserisci Parte Di Ricambio";
      this.update = false;
      this.parte = new ParteRicambio();
      console.log("la parte NON esiste nel db quindi verrà creata");
    }
  }

  modificaParte() {
    // this.parte2.matricola = "ciao";
    // console.log(this.parte2.matricola);
    if (this.update === true) {
      console.log(this.parte);
      this.partiService
        .aggiorna(this.parte)
        .subscribe((response) => this.handleResponse(response));
    } else {
      console.log("sono nell else modific aparte()");

      this.partiService
        .aggiorna(this.parte)
        .subscribe((response) => this.handleResponse(response));
    }

    alert("Parte Ricambio Aggiornata!");
    this.route.navigate(["partiPage"]);
  }
  handleResponseByMatr(response) {
    console.log(response);
    this.parte = response;
  }

  handleResponse(response) {
    console.log(response);
    this.parte = response;
  }
}
