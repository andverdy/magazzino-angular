import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Cliente } from "../../model/Cliente";
import { ClientiService } from "../../services/clienti.service";

@Component({
  selector: "app-form-cliente",
  templateUrl: "./form-cliente.component.html",
  styleUrls: ["./form-cliente.component.css"],
})
export class FormClienteComponent implements OnInit {
  cliente: Cliente;
  insert: boolean;
  isUpd = sessionStorage.getItem("id");
  update: boolean;
  public h1: any = "Inserisci Cliente";
  constructor(private route: Router, private clienteService: ClientiService) {}

  ngOnInit(): void {
    let id = JSON.parse(sessionStorage.getItem("id"));

    if (id != null) {
      this.h1 = "Aggiorna Cliente";
      this.update = true;
      // this.parte = history.state.data;
      console.log("il cliente esiste nel db quindi verrà aggiornata");
      this.clienteService
        .getClienteById(id)
        .subscribe((response) => this.handleResponseById(response));
    } else {
      this.h1 = "Inserisci Cliente";
      this.update = false;
      this.cliente = new Cliente();
      console.log("il cliente NON esiste nel db quindi verrà creata");
    }
  }

  modificaCliente() {
    if (this.update === true) {
      this.clienteService
        .aggiorna(this.cliente)
        .subscribe((response) => this.handleResponse(response));
    } else {
      this.clienteService
        .aggiorna(this.cliente)
        .subscribe((response) => this.handleResponse(response));
    }

    alert("Cliente Aggiornato!");
    this.route.navigate(["clientiPage"]);
  }
  handleResponseById(response) {
    console.log(response);
    this.cliente = response;
  }

  handleResponse(response) {
    console.log(response);
    this.cliente = response;
  }
}
