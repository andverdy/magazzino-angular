import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
} from "@angular/core";
import { Router } from "@angular/router";
import { LoginService } from "src/app/services/login.service";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css"],
})
export class NavbarComponent implements OnInit {
  filter: string = "";
  username: string;
  isAuth = sessionStorage.getItem("token");
  private email: string;
  constructor(public auth: LoginService, private router: Router) {}

  ngOnInit() {
    this.username = sessionStorage.getItem("username");
  }

  logOut() {
    console.log("click fatto");
    sessionStorage.removeItem("token");
    sessionStorage.removeItem("username");
    sessionStorage.removeItem("matricola");

    window.location.href = "http://localhost:4200/";
  }

  magazzPage() {
    this.router.navigate(["partiPage"]);
  }
}
