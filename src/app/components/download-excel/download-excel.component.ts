import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { PartiServiceService } from "../../services/parti-service.service";

@Component({
  selector: "app-download-excel",
  templateUrl: "./download-excel.component.html",
  styleUrls: ["./download-excel.component.css"],
})
export class DownloadExcelComponent implements OnInit {
  constructor(
    private partiService: PartiServiceService,
    private router: Router
  ) {}

  ngOnInit(): void {
    console.log("sto entrando nel exc comp");
    this.partiService.downloadExcelFile();
    this.router.navigate(["partiPage"]);
  }
}
