import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Cliente } from "src/app/model/Cliente";
import { ClientiService } from "src/app/services/clienti.service";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "app-cliente",
  templateUrl: "./cliente.component.html",
  styleUrls: ["./cliente.component.css"],
})
export class ClienteComponent implements OnInit {
  @Input() cliente: Cliente;
  clienti: Array<Cliente> = [];
  roleIsAuth: boolean;
  constructor(
    private userService: UserService,
    private clientiService: ClientiService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.userService
      .roleIsAuth()
      .subscribe((response) => this.handleResponseUser(response));

    this.clientiService
      .getClienti()
      .subscribe((response) => this.handleResponse(response));
  }

  visualizzaFormModifica(id: number) {
    if (id != null) {
      sessionStorage.setItem("id", JSON.stringify(id));
      this.router.navigate(["/aggiornaCliente"]);
    }
  }

  visualizzaFormInserimento() {
    sessionStorage.removeItem("id");
    this.router.navigate(["/aggiornaCliente"]);
  }

  handleResponse(response: Array<Cliente>) {
    response.forEach((cliente: Cliente) => {
      this.clienti.push(cliente);
    });
  }

  handleResponseUser(response) {
    this.roleIsAuth = response;
  }
}
