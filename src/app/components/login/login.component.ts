import { HttpHeaders } from "@angular/common/http";
import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import { User } from "src/app/model/User";
import { LoginService } from "src/app/services/login.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  @Output() userlogout = new EventEmitter();
  constructor(private auth: LoginService, private router: Router) {}
  ngOnInit(): void {}
  user: User;
  username = "";
  password = "";
  autenticato = true;
  idUser: number;
  private isUserLogged = false;
  errorMsg =
    "Nuovo utente registrato, clicca su entra per accedere al magazzino!";

  gestAuth() {
    this.auth.autenticaService(this.username, this.password).subscribe(
      (data) => {
        console.log(data);
        this.autenticato = true;
        this.isUserLogged = true;
        this.router.navigate(["partiPage"]);
      },
      (error) => {
        console.log(error);
        this.autenticato = false;
        this.isUserLogged = false;

        if (this.isUserLogged === false) {
          alert(
            "Utente non presente, premi ok per confermare la registrazione!"
          );

          //this.router.navigate(["/"]);
          this.auth
            .registrati(this.username, this.password)
            .subscribe((response) => this.handleResponse(response));

          //sessionStorage.removeItem("token");
        }
      }
    );
  }

  handleResponse(response) {
    console.log("sono nell handel registrati");
    console.log(response);
    this.user = response;
    this.auth
      .getIdByUsername(this.username)
      .subscribe((response) => this.handleRespId(response));
  }
  handleRespId(response) {
    console.log(response);
    this.idUser = response;

    this.auth
      .saveRole(this.idUser)
      .subscribe((response) => this.handleRespSaveRole(response));
  }

  handleRespSaveRole(response) {
    console.log("cazzo porco cane salva il ruolo");
    console.log(response);
  }
}
