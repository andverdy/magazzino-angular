import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ClienteComponent } from "./components/cliente/cliente.component";
import { FormPartiComponent } from "./components/formParti/formParti.component";

import { LoginComponent } from "./components/login/login.component";
import { PartiRicambioComponent } from "./components/parti-ricambio/parti-ricambio.component";
import { DownloadExcelComponent } from "./components/download-excel/download-excel.component";
import { FormClienteComponent } from "./components/form-cliente/form-cliente.component";

const routes: Routes = [
  { path: "", component: LoginComponent },
  { path: "partiPage", component: PartiRicambioComponent },
  { path: "clientiPage", component: ClienteComponent },
  { path: "aggiornaParte", component: FormPartiComponent },
  { path: "aggiornaCliente", component: FormClienteComponent },
  { path: "downExcelPage", component: DownloadExcelComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
